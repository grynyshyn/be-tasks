'use strict';

var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');
var base = module.superModule;

server.extend(base);
server.append('Show', function (req, res, next) {


    var viewData = res.getViewData();
    viewData.age = 33;
    res.setViewData(viewData);
    
   // res.render('/home/homePage');
    next();
});

module.exports = server.exports();
