'use strict';

var baseAdress = module.superModule;

/**
 * Address class that represents an orderAddress
 * @param {dw.order.OrderAddress} addressObject - User's address
 * @constructor
 */
function address(addressObject) {
    baseAdress.call(this, addressObject);

    if (this.address) {
        this.address.addressType = addressObject.custom.addressType;
    }
}

module.exports = address;
