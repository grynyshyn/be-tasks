'use strict';
var baseFullProduct = module.superModule;

function fullProductModel(product, apiProduct, options) {
    var baseProduct = baseFullProduct(product, apiProduct, options);
    baseProduct.rand = 55;
    return baseProduct;
}

module.exports = fullProductModel;

