'use strict';

var baseAccountModel = module.superModule;

/**
 * Extention for Account class that represents the current customer's profile dashboard
 * mixes in hobby, petname and favouriteMusic attributes
 *
 * @param {Object} currentCustomer - Current customer
 * @param {Object} addressModel - The current customer's preferred address
 * @param {Object} orderModel - The current customer's order history
 * @constructor
 */
function extendedAccountModel(currentCustomer, addressModel, orderModel) {
    baseAccountModel.call(this, currentCustomer, addressModel, orderModel);

    var profileCustom = currentCustomer.raw.profile.custom;

    this.profile.hobby = profileCustom.hobby;
    this.profile.petname = profileCustom.petname;
    this.profile.favouriteMusic = profileCustom.favouriteMusic;
}

module.exports = extendedAccountModel;
