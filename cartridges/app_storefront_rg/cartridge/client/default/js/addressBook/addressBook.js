'use strict';

var adressBook = require('base/addressBook/addressBook');

adressBook.habdleToggleAddressType = function () {
    $('#addresTypeFormTabs [data-toggle="tab"]').on('show.bs.tab', function (e) {
        var addressType = $(e.target).data().addresstype;

        $('#addressType').val(addressType);
      });
};

module.exports = adressBook;
